package com.tt.expandablecalendar.ui.widget.calendar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tt.expandablecalendar.R;

/**
 * 日历的适配器 <功能简述> <Br>
 * <功能详细描述> <Br>
 * 
 * @author kysonX http://www.hizhaohui.cn/
 */
public class CalendarGridViewAdapter extends BaseAdapter {
    /**
     * 自定义监听接口
     * 
     * @author Administrator
     * 
     */
    public interface OnDaySelectListener {
        void onDaySelectListener(Calendar date);
    }

    private OnDaySelectListener mOnDaySelectListener;

    private List<CalendarItem> mDatas = new ArrayList<CalendarItem>();
    // 当前选中的日期
    private Calendar mSelectedCal;

    private LayoutInflater mInflater;

    private Context mContext;

    public CalendarGridViewAdapter(Context context) {
        this.mContext = context;
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mDatas.size();
    }

    @Override
    public CalendarItem getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GridViewHolder holder;
        if (convertView == null) {
            holder = new GridViewHolder();
            convertView = mInflater.inflate(
                    R.layout.widget_common_calendar_gridview_item, parent,
                    false);
            holder.tvDay = (TextView) convertView
                    .findViewById(R.id.widget_common_calendar_gridview_item_date);
            convertView.setTag(holder);
        } else {
            holder = (GridViewHolder) convertView.getTag();
        }
        final CalendarItem calendarItem = getItem(position);
        holder.tvDay.setText(String.valueOf(calendarItem.calendar
                .get(Calendar.DAY_OF_MONTH)));
        holder.tvDay.setTextAppearance(mContext, getTextStyle(calendarItem));
        holder.tvDay
                .setSelected(isSameDay(mSelectedCal, calendarItem.calendar));
        holder.tvDay
                .setEnabled(calendarItem.monthPos == CalendarItem.MONTH_CURRENT);
        holder.tvDay.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                checkItem(calendarItem.calendar);
            }
        });
        return convertView;
    }

    // check one item
    private void checkItem(Calendar cal) {
        // 点击的和当前选中的是同一天
        if (isSameDay(cal, mSelectedCal)) {
            return;
        }
        mSelectedCal = cal;
        notifyDataSetChanged();
        if (mOnDaySelectListener != null) {
            mOnDaySelectListener.onDaySelectListener(mSelectedCal);
        }
    }

    /**
     * get textview's color <功能简述>
     * 
     * @param calendarItem
     * @return
     */
    private int getTextStyle(CalendarItem calendarItem) {
        int style;
        if (calendarItem.monthPos == CalendarItem.MONTH_CURRENT) {
            // current month
            if (calendarItem.isToday) {
                style = R.style.textView_14_green_bold;
            } else {
                style = R.style.textView_12_white;
            }
        } else {
            // 非本月
            style = R.style.textView_12_grey_light;
        }
        return style;
    }

    public static class GridViewHolder {
        public TextView tvDay;
    }

    public void setOnDaySelectListener(OnDaySelectListener onDaySelectListener) {
        this.mOnDaySelectListener = onDaySelectListener;
    }

    public List<CalendarItem> getDatas() {
        return mDatas;
    }

    public void setDatas(List<CalendarItem> datas) {
        this.mDatas = datas;
    }

    public void setSelectedDate(Calendar cal) {
        checkItem(cal);
    }

    public Calendar getSelecterDate() {
        return mSelectedCal;
    }

    /**
     * 比较两个日期是否为同一天 <功能简述>
     * 
     * @param fromCalendar
     * @param toCalendar
     * @return
     */
    public static boolean isSameDay(Calendar fromCalendar, Calendar toCalendar) {
        if (fromCalendar == null || toCalendar == null) {
            return false;
        }
        // 年月日都一样，则为同一天
        return fromCalendar.get(Calendar.YEAR) == toCalendar.get(Calendar.YEAR)
                && fromCalendar.get(Calendar.MONTH) == toCalendar
                        .get(Calendar.MONTH)
                && fromCalendar.get(Calendar.DAY_OF_MONTH) == toCalendar
                        .get(Calendar.DAY_OF_MONTH);
    }

}
